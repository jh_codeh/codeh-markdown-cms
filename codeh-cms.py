# -*- coding: utf-8 -*-
__author__ = 'jh'
__copyright__ = 'www.codeh.de'

import os
import config as config
import logging
from logging.handlers import RotatingFileHandler
from flask import Flask, render_template
from flask.ext.misaka import markdown, Misaka
from flask.ext.cache import Cache

cache = Cache(config={'CACHE_TYPE': 'simple'})
app = Flask(__name__)
app.debug = False
app.secret_key = '3B45BE9C8CAB2CA51B99B942D5D4CF00BAR'
Misaka(app)
cache.init_app(app)

# Returns the file list for the menu
# todo: must sort this foo
def getFileList():
    menuList = []
    fileList = os.listdir(config.PATH)
    for file in fileList:
        if file.endswith('.txt'):
            menuList.append(file[:-4])
    return menuList


# Returns the specific page
def getPage(page=config.INDEXPAGE):
    filePath = config.PATH + page + '.txt'
    try:
        with open(filePath, 'r') as file:
            fileContent = file.read().decode('utf8')
    except:
        app.logger.error('Page %s not found!', page)
        return 'Sorry, page not found!'
        file.close()
    return markdown(fileContent)


@cache.cached(timeout=50)
@app.route('/')
def index():
    if not os.path.exists(config.PATH):
        return page_not_found()
    return render_template('index.html', pages=getFileList(), content=getPage(), pageTitle=config.PAGETITLE,
                           copyright=config.COPYRIGHT)


@cache.cached(timeout=50)
@app.route('/<page>')
def show_page(page):
    return render_template('index.html', pages=getFileList(), content=getPage(page), pageTitle=config.PAGETITLE,
                           copyright=config.COPYRIGHT)


@app.errorhandler(404)
def page_not_found():
    app.logger.error('Page not found!')
    return render_template('page_not_found.html'), 404


if __name__ == '__main__':
    handler = RotatingFileHandler(config.LOGFILE, maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    app.run()
