# -*- coding: utf-8 -*-
__author__ = 'jh'
__copyright__ = 'www.codeh.de'

# This is the conifg file for your nice web application.

# Please enter here the path to the txt files with your content.
PATH = '<to the files>'

# Log Path
LOGFILE = '<install path>/log-files/cmcms.log'

# Index (Startpage) the name of the first page.
INDEXPAGE = 'Index'

# Page title.
PAGETITLE = 'super simple CMS'

# Enter your name / company name.
COPYRIGHT = 'Your Copyright'