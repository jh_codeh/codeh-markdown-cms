codeH super simple markdown CMS

This CMS system based on Python, Flask and some flask extension. It is free and open under MIT license.

Super simple?

This means you need no Login system, no database and no programmer skills. Little administration skills, but feel free to contact me, after reading the install how to. ;)

This cms uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax.
For best practice you have a Dropbox account and installed the Dropbox app on your server. If not and don't won't create a new or something you like. Dropbox or some other sync tool make your web life simpler as it can. You edit your content in the .txt files with markdown syntax. Put the path to the files into the cms config and enjoy!

Installation

If you know a flask based python app installation on a server, do what you do. If not, read this please.
Based on apache2 web server.

Please check the following points, must be installed:
apache2 with mod_wsgi http://code.google.com/p/modwsgi/
Python 2.7
flask with the following extensions (simples way install with pip):
http://flask.pocoo.org
http://pythonhosted.org/Flask-Cache/
https://pypi.python.org/pypi/Flask-Misaka


So how install a python app on your server. 
Upload all files from this repository into: /usr/local/www/<codeh-cms> if path not exists create it.

Open the config.py and follow the statements in it.

After this go to /etc/apache2/sites-available 
Create a file and name it <codeh-cms>, content from the file:

<VirtualHost *:80>
    ServerName <your domain><or subdomain, like this: codeh-cms.codeh.de>
    DocumentRoot <your web path, looks like this /var/www/codeh-cms>
    
    WSGIDaemonProcess <codeh-cms, change to what you wont> user=www-data group=www-data processes=1 threads=5
    WSGIScriptAlias / <path to the app where you uploaded this repository files, like: /usr/local/www/<codeh-cms>/<codeh-cms>.wsgi>>
    
    <Directory "/usr/local/www/codeh-cms">
        WSGIProcessGroup codeh-cms
        WSGIApplicationGroup %{GLOBAL}
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>

Your eagle eye does not deceive you, there is a codeh-cms.wsgi file in the application folder. If you config the paths like in the guidance, we don't touch it. If not, you must edit the path.

import sys, os

sys.path = ['</usr/local/www/codeh-cms>'] + sys.path
os.chdir(os.path.dirname(__file__))

from codeh-cms import app as application

Fire up the following: sudo a2ensite
Reload your apache2 and enjoy your new page: codeh-cms.<your domain>.<what ever>

Sometimes you must change a little bit the access authorization, but this is simple and google is your friend, if not feel free to write me.

Web-style
The web site use on of the bootstrap standard styles. To change it only change the files in the static/css and static/js area. But don't forget the templates/index.html file.

Best practice
The simples way to use this cms is, with a Dropbox account! So you edit your content on your local machine and Dropbox and the cms do the rest.


Beer
If it works and you enjoy, some feedback would be really nice!
Please leave the codeh.de tag on your page and if not a donation would be really nice :)
Respect the license.

Have fun!